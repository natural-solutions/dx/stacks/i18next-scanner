FROM node:lts-alpine AS development
ENV NODE_ENV development

# Check https://github.com/nodejs/docker-node/tree/b4117f9333da4138b03a546ec926ef50a31506c3#nodealpine to understand why libc6-compat might be needed.
RUN apk add --no-cache libc6-compat 
# Add a work directory
WORKDIR /src
# Cache and Install dependencies
COPY package.json ./
RUN yarn install --frozen-lockfile
# Copy app files
COPY . .
# Expose port
EXPOSE 3000
# Start the app
CMD [ "yarn", "start" ]