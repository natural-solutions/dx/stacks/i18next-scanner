import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import DocumentPage from "./pages/document";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
// import i18n (needs to be bundled ;))
import "./i18n";
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <React.StrictMode>
    <Router>
      <Routes>
        <Route exact path="/" element={<App />} />
        <Route exact path="/document" element={<DocumentPage />} />
      </Routes>
    </Router>
  </React.StrictMode>
);
