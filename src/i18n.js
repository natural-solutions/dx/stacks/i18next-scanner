import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import LanguageDetector from "i18next-browser-languagedetector";
import CommonEN from "./locales/en/common.json";
import CommonFR from "./locales/fr/common.json";
import PagesEN from "./locales/en/pages.json";
import PagesFR from "./locales/fr/pages.json";

i18n
  .use(LanguageDetector)
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    preload: ["en", "fr"],
    lng: "fr",
    fallbackLng: "fr",
    ns: ["common", "pages"],
    nsSeparator: ":",
    defaultNS: "common",
    resources: {
      en: {
        common: CommonEN,
        pages: PagesEN,
      },
      fr: {
        common: CommonFR,
        pages: PagesFR,
      },
    },
    interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
    },
    react: {
      useSuspense: false,
    },
  });

export default i18n;
