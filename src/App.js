import logo from "./logo.svg";
import "./App.css";
import { Header } from "./components/layout/Header";
import { useTranslation } from "react-i18next";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import LanguageSelector from "./components/layout/LanguageSelector";

function App() {
  const { t, i18n } = useTranslation(["pages"]);
  const navigation = useNavigate();

  useEffect(() => {
    i18n.changeLanguage(navigation.locale);
  }, [navigation.locale, i18n]);

  return (
    <div className="App">
      <Header />
      <img src={logo} className="App-logo" alt="logo" />
      <LanguageSelector />
      <h1 class="text-3xl font-bold text-sky-500 underline">
        {t("pages:homepage.title")}
      </h1>
      <p>{t("pages:homepage.subtitle")}</p>
      <p>{t("pages:homepage.text")}</p>
    </div>
  );
}

export default App;
