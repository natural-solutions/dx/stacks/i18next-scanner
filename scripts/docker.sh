#!/usr/bin/env bash

set -e

# Pre-pre-flight? 🤷
if [[ -n "$MSYSTEM" ]]; then
    echo "Seems like you are using an MSYS2-based system (such as Git Bash) which is not supported. Please use WSL instead.";
    exit 1
fi

PROJECT_NAME="i18next-scanner"


    docker-compose --project-name=${PROJECT_NAME} -f ./docker-compose.dev.yml "$@"
