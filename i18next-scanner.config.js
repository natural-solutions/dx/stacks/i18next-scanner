const fs = require("fs");
const chalk = require("chalk");

module.exports = {
  input: [
    "src/*.js",
    "src/components/**/*.js",
    "src/pages/**/*.js",
    // Use ! to filter out files or directories
    "!app/**/*.spec.{js}",
    "!app/i18n/**",
    "!**/node_modules/**",
  ],
  output: "./",
  options: {
    debug: true,
    func: {
      list: ["t"],
      extensions: [".js"],
    },
    lngs: ["en", "fr"],
    ns: ["common", "pages"],
    defaultLng: "fr",
    defaultNs: "common",
    defaultValue: "__TEXTE_NON_TRADUIT__",
    resource: {
      loadPath: "src/locales/{{lng}}/{{ns}}.json",
      savePath: "src/locales/{{lng}}/{{ns}}.json",
      jsonIndent: 2,
      lineEnding: "\n",
    },
    nsSeparator: ":", // namespace separator
    keySeparator: ".", // key separator
    interpolation: {
      prefix: "{{",
      suffix: "}}",
    },
  },
  transform: function customTransform(file, enc, done) {
    "use strict";
    const parser = this.parser;
    const content = fs.readFileSync(file.path, enc);
    let count = 0;
    let ns;
    const match = content.match(/useTranslation\(.+\)/);
    if (match) ns = match[0].split(/(\'|\")/)[2];

    parser.parseFuncFromString(content, { list: ["t"] }, (key, options) => {
      // Add keys based on metadata (dynamic or otherwise)
      parser.set(
        key,
        Object.assign({}, options, {
          // if match with ns, the key will be entered into the good ns. if not match : default ns
          ns: ns ? ns : DEFAULT_NS,
        })
      );
      ++count;
    });

    if (count > 0) {
      console.log(
        `i18next-scanner: count=${chalk.cyan(count)}, file=${chalk.yellow(
          JSON.stringify(file.relative)
        )}`
      );
    }

    done();
  },
};
